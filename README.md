### About ###

Front-end webapp to display a user's Github profile information.

A text-field is presented to the User to enter a known Github username.  Upon submission of that username the webapp checks if the name exists in client-side cache, if not, a call is made to Github's api to retrieve the username's user-info and also the user's public repositories.  Assuming that the username exists, the webapp will then display that information on the webpage after caching the data.  Cached data expires after 5 minutes. 

React is used to manage the View as well as serve as the Controller and Model.  Each of these components are wrapped together within their respepctive domain:  App, Profile, and Search.

/src/App

* App.js
    -  App serves as the starting point into the webapp coordinating it's children components Profile and Search.  Included in this file is a second class, Header, which provides the header to the webapp.  App, and the second class Header, define the overall page.  Additionally, App relays communication between the sibling components Search and Profile. 
  * Search.js
    -  Search is responsible for the View of the search bar and serves as a Controller in managing the User input for that search bar.  Search also conducts the API call to Github to retrieve the User data and manages the caching of the data. 
  * Profile.js
    -  Profile handles the View for the User profile.  In difference to the previous two components, Profile is purely a View.
  * Styles
    -  App and Profile both have corresponding style sheets:  App.css and Profile.css
    
## Running The Project

Make sure you have [Node.js](https://nodejs.org/en/) installed. Also, if you do not have [Bower](https://bower.io/) installed install it after installing node by running `npm install -g bower`.

Then run in the project directory run the following commands to serve the app into the browser:

```bash
$ npm install
$ npm start
```