import React, { Component } from 'react';

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
    this.findUser = this.findUser.bind(this);
  }

  propTypes() {
    returnUser: React.PropTypes.func
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  findUser(event) {
    event.preventDefault();
    this.callGithub(this.state.value);
    this.setState({value: ''});
  }

  checkCache(userName) {
    let expiration = 5*60; 
    let cache = localStorage.getItem(userName);
    let timeCached = localStorage.getItem(userName + ':time');

    if (cache !== null && timeCached !== null) {
      let age = (Date.now() - timeCached) / 1000;
      if (age < expiration) {
        this.props.returnUser(JSON.parse(cache));
        return true;
      }
      else {
        localStorage.removeItem(userName);
        localStorage.removeItem(userName + ':time');
      }
    }
    return false;
  }

  callGithub(userName) {
    if (this.checkCache(userName))
      return;

    fetch('https://api.github.com/users/'+userName).then(response => {
        fetch('https://api.github.com/users/'+userName+'/repos').then(repos => {
          response.clone().json().then(user => {
            repos.clone().json().then(repos => {
              var date = new Date(user.created_at);
              date = (date.getMonth()+1)+'/'+date.getDate()+'/'+date.getFullYear();
                var toStore = {
                  'location': user.location,
                  'public_repos': user.public_repos,
                  'name': user.name,
                  'userName': user.login,
                  'url': user.html_url,
                  'email': user.email,
                  'bio': user.bio,
                  'createdAt': date,
                  'avatar': user.avatar_url,
                  'repos': repos
                }
                localStorage.setItem(userName, JSON.stringify(toStore));
                localStorage.setItem(userName+':time', Date.now());
                this.props.returnUser(toStore);
            })
          })
        });
      }
    )
  }

  render() {
    return (
      <form onSubmit={this.findUser}>
        User Name:  <input type="text" ref="inputBox" value={this.state.value} onChange={this.handleChange} placeholder="Enter the user name here!"/>
        <input type="submit" value="Find User!"/>
      </form>
    );
  }
}

export default Search;