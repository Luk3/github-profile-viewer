import React, { Component } from 'react';
import './Profile.css';
import Search from './Search';

class Profile extends Component {
	constructor() {
	    super();
	    this.state = null;
  	}

  	getRepos() {
  		var repos = [];

		for (var i in this.props.userData.repos) {
			repos.push(<div className="repo"><a href={this.props.userData.repos[i].html_url}>{this.props.userData.repos[i].name}</a><br/></div>);
		}

		return repos;
  	}

	render() {
		
		var repos = this.getRepos();

		if (this.props.userData.userName == null)
			return (
			<div className="invalidUser">
				User Not Found <br/><br/>
				Please Try Again
			</div>
		)

	    return (
	    	<div className="profile">
		    	<div className="profilePicture"> <a href={this.props.userData.url}><img src={this.props.userData.avatar} width="100%"/></a> </div>
		    	<div className="stats">
		    		<div className="userName"> <div className="label">User Name:  </div> {this.props.userData.userName} </div><br/>
		    		<div className="realName"> <div className="label">Real Name:  </div> {this.props.userData.name} </div><br/>
		    		<div className="location"> <div className="label">Location:  </div> {this.props.userData.location} </div><br/>
		    		<div className="email"> <div className="label">Email:  </div> {this.props.userData.email} </div><br/>
		    		<div className="memberSince"> <div className="label">Member Since:  </div> {this.props.userData.createdAt} </div><br/>
		    		<div className="bio"> <div className="label">Bio:  </div> {this.props.userData.bio} </div><br/>
		    	</div>
		    	<div className="repos">
		    		<div className="repoTitle"> Repos </div>
		    		{repos}
		    	</div>
	    	</div>
	    );
	  }
}

export default Profile;