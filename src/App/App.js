import React, { Component } from 'react';
import './App.css';
import Search from './Search'
import Profile from './Profile'

class App extends Component {
  constructor() {
    super();
    this.state = null;
  }

  userData(data) {
    this.state = data;
    this.setState(this.state);
  }

  render() {
    var profile = "";

    if (this.state!=null) {
      profile = (<Profile userData={this.state} />);
    }

    return (
      <div>
        <Header />
        <div className="app">
          <p>
            <h2>Find a user by his/her Github user name.</h2>
            Simply enter the user's handle into the search field...
          </p>
          <Search returnUser={this.userData.bind(this)} />
          {profile}
        </div>
      </div>
    );
  }
}

class Header extends Component {
  render() {
    return (
      <div className="header">
        GitHub Profile Puller
      </div>
    );
  }
}

export default App;
